import { useState } from "react"
import { CommonUtils } from "../../utils/common.utils"

const HeaderComponent = () => {
    const [user, setUser] = useState(CommonUtils.getUserEmail())

    return (
        <div className="w-full h-16 bg-amber-400 flex items-center justify-between relative">
            <div className="rounded-full bg-amber-400 w-48 text-center h-28">
                <div className="flex items-center h-32 ">
                    <h2 className="text-xl font-extrabold text-white text-gray-800 text-zinc-50 mt-4 ml-7 rounded-full shadow-black shadow-md p-2">RS-Group 3</h2>
                </div>
            </div>
            <div>
                <ul className="flex space-x-4 mr-20">
                    <li>
                        <a href="/" className="text-yellow-950 hover:text-gray-700 text-lg font-bold">Home</a>
                    </li>
                    <li>
                        <a href="/ManagerSchedule" className="text-yellow-950 hover:text-gray-700 text-lg font-bold">Schedule</a>
                    </li>
                    <li>
                        <a href="/Order" className="text-yellow-950 hover:text-gray-700 text-lg font-bold">Order</a>
                    </li>
                    <li>
                        <a href="/Course" className="text-yellow-950 hover:text-gray-700 text-lg font-bold">Course</a>
                    </li>
                    <li>
                        <a href="/Food" className="text-yellow-950 hover:text-gray-700 text-lg font-bold">Food</a>
                    </li>
                    
                    <li>
                        <label>{(user != null) ? 'Welcome back, ' + user : ''}</label>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default HeaderComponent