import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebook, faInstagram, faTwitter } from "@fortawesome/free-brands-svg-icons";

const FooterComponent = () => {
    return (
        <div className="w-full px-4 py-8 bg-red-900 grid grid-cols-12">
            <div className="flex justify-between items-center col-span-3">
                <div className="flex items-center space-x-4">
                    <div className="hidden lg:flex">
                        <FontAwesomeIcon icon={faFacebook} className="px-4 w-6 h-6"/>
                        <FontAwesomeIcon icon={faInstagram} className="px-4 w-6 h-6"/>
                        <FontAwesomeIcon icon={faTwitter} className="px-4 w-6 h-6"/>
                    </div>
                </div>
            </div>
            <div className="hidden lg:flex col-span-8 ml-10 italic font-bold">
                <h4 className="text-gray-200 hover:text-white px-4 py-2 block">Phone : +84 xxx-xxx-xxx</h4>
                <h4 className="text-gray-200 hover:text-white px-4 py-2 block">Address : xxxxxx-xxxxxx-xxxxx-xxxxxx</h4>
            </div>
            <a href="#" className="text-xl font-bold text-white italic col-span-1">RS-Group 3</a>
        </div>
    )
}
export default FooterComponent;