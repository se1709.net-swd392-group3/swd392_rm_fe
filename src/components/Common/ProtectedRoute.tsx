import { Navigate } from "react-router-dom";
import { CommonUtils } from "../../utils/common.utils";

export const ProtectedRoute = ({
    children,
  }) => {
    const user = CommonUtils.getUserEmail()
    if (!user) {
      return <Navigate to='/auth/login' replace />;
    }
    
    return children;
  };