import React from "react";


const ModalAlert = ({ isvisible, Close }) => {
    if(!isvisible) return null;

    return (
        <div className="fixed inset-0 Obg-black bg-opacity-25 backdrop-blur-sm flex justify-center items-center" >
            <div className="w-[600px] flex flex-col">
                <button onClick={() => Close()} className="text-white font-bold text-2xl place-self-end bg-green-500 w-full text-end pr-5 py-1">X</button>
                <div className="bg-white p-2 rounded text-center text-2xl font-bold">Success</div>
            </div>
        </div>
    )
}
export default ModalAlert;