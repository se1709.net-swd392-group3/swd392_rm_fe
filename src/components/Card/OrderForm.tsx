const OrderForm = () => {
    return (
        <div className="container mx-auto px-4 py-8">
            <div className="grid grid-cols-5 gap-4">
                <div className="col-span-2">
                    <div className="text-2xl font-bold my-5">Food Item:</div>
                    <div className="grid grid-cols-5 gap-4">
                        <label className="block text-sm font-bold mb-2 col-span-1 mt-2">
                            Food Item 1 :
                        </label>
                        <select id="food-item-1" name="food-item-1" className="col-span-4 appearance-none block w-full px-3 py-2 text-base border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500">
                            <option value="">Select item</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                        </select>
                    </div>

                    <div className="grid grid-cols-5 gap-4 my-5">
                        <label className="block text-sm font-bold mb-2 col-span-1 mt-2">
                            Food Item 2
                        </label>
                        <select id="food-item-2" name="food-item-2" className="col-span-4 appearance-none block w-full px-3 py-2 text-base border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500">
                            <option value="">Select item</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                        </select>
                    </div>

                    <div className="grid grid-cols-5 gap-4 my-5">
                        <label className="block text-sm font-bold mb-2 col-span-1 mt-2">
                            Food Item 3
                        </label>
                        <select id="food-item-2" name="food-item-2" className="col-span-4 appearance-none block w-full px-3 py-2 text-base border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500">
                            <option value="">Select item</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                        </select>
                    </div>

                </div>

                <div className="grid grid-cols-2 gap-4 col-span-3">
                    <div className="">
                        <h2 className="text-2xl font-bold my-5">Quantity:</h2> 
                        <div>
                            <input type="number" id="quantity" name="quantity" className="appearance-none block w-full px-3 py-2 text-base border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" />
                        </div>
                        <div>
                            <input type="number" id="quantity" name="quantity" className="my-5 appearance-none block w-full px-3 py-2 text-base border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" />
                        </div>
                        <div>
                            <input type="number" id="quantity" name="quantity" className="my-5 appearance-none block w-full px-3 py-2 text-base border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" />
                        </div>
                    </div>
                    <div>
                        <h2  className="text-2xl font-bold my-5">Price:</h2>
                        <div>
                            <input type="number" id="quantity" name="quantity" className="appearance-none block w-full px-3 py-2 text-base border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" />
                        </div>
                        <div>
                            <input type="number" id="quantity" name="quantity" className="my-5 appearance-none block w-full px-3 py-2 text-base border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" />
                        </div>
                        <div>
                            <input type="number" id="quantity" name="quantity" className="my-5 appearance-none block w-full px-3 py-2 text-base border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}
export default OrderForm;