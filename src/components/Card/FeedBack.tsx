import { faFaceGrinHearts } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "@material-tailwind/react";

const FeedBackCard= () =>{
    return(
        <div className="bg-teal-100 w-4/5 h-auto rounded-3xl">
            <h3 className='text-center p-2 text-3xl my-10 pt-10 mx-10'>
                 Luôn mong cầu được tiếp nhận ý kiến từ khách hàng.
            </h3>
            <div className="mx-36">
                <FontAwesomeIcon icon={faFaceGrinHearts} className="w-10 h-10 mx-5"/>
                <FontAwesomeIcon icon={faFaceGrinHearts} className="w-10 h-10 mx-5"/>
                <FontAwesomeIcon icon={faFaceGrinHearts} className="w-10 h-10 mx-5"/>
            </div>
            <Button about="/FeedBack" placeholder="Feed Back" className="w-60 h-30 text-white my-10 mx-36">
                <a href="/FeedBack" className="w-60 h-30">Feed Back </a>
            </Button>
        </div>
    )
}
export default  FeedBackCard;