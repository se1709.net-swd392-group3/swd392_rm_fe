import menu from '../../Image/menu.jpg'

const IntroMenu = () => {
    return (
        <div className="bg-teal-100 w-3/5 h-auto rounded-3xl border border-collapse border-black shadow-black shadow-md">
            <img src={menu} className='w-4/5 h-4/5 py-10' />
            <h3 className='text-center text-3xl p-2'>
                Thực đơn vì khách hàng
            </h3>
        </div>
    )
}
export default IntroMenu;