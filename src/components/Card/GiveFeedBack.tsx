import { useState } from "react";
import ModalAlert from "./ModalAlert";

const GiveFeedBacl = () => {
    const [modalIsOpen, setIsOpen] = useState(false)

    const CloseModal = () =>{
        setIsOpen(false);
    }
    
    return (
        <div className="w-3/5 h-4/5 mx-auto px-4 py-8">
            <div className="flex flex-col bg-gray-200 rounded-lg shadow-md px-4 py-6">
                <h2 className="text-xl font-bold mb-4">Give Feedback</h2>
                <div className="mb-4">
                    <label className="block text-sm font-bold mb-2">Rate the Product:</label>
                    <select id="rating" name="rating" className="appearance-none block w-full px-3 py-2 text-base border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500">
                        <option value="">Select a rating</option>
                        <option value="5">5 Stars - Excellent</option>
                        <option value="4">4 Stars - Good</option>
                        <option value="3">3 Stars - Average</option>
                        <option value="2">2 Stars - Poor</option>
                        <option value="1">1 Star - Very Poor</option>
                    </select>
                </div>
                <div className="mb-6">
                    <label className="block text-sm font-bold mb-2">Comment:</label>
                    <textarea id="comment" name="comment" className="appearance-none block w-full px-3 py-2 text-base border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"></textarea>
                </div>
                <button onClick={() => {setIsOpen(true)}} type="submit" className="w-full py-2 px-4 bg-indigo-500 text-white font-bold rounded-md hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Submit</button>
            </div>
            <ModalAlert isvisible={modalIsOpen} Close={CloseModal}/>
        </div>

    )
}
export default GiveFeedBacl;