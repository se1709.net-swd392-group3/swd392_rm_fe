import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import chef from '../../Image/chef.jpg'
import { faStar } from '@fortawesome/free-regular-svg-icons';

const IntroChef = () => {
    return (
        <div className="bg-teal-100 w-3/5 h-auto rounded-3xl border border-collapse border-black shadow-black shadow-md">
            <img src={chef} className='w-4/5 h-4/5 py-10' />
            <h3 className='text-center p-2 text-2xl'>
                Đầu bếp đẳng cấp thế giới
            </h3>
            <div className='text-center p-2'>
                <FontAwesomeIcon icon={faStar} className="w-10 h-10" />
                <FontAwesomeIcon icon={faStar} className="w-10 h-10" />
                <FontAwesomeIcon icon={faStar} className="w-10 h-10" />
            </div>
        </div>
    )
}
export default IntroChef;