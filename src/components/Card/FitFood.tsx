const FitFood = () => {
    return (
        <div className="container mx-auto px-4 py-8 ">
            <div className="flex flex-wrap grid grid-rows-6 bg-green-500 w-2/5 p-6 rounded-xl">
                <div className="w-full px-2 row-span-2 grid-cols-2 my-5 m-auto grid grid-cols-4">
                    <label className="block text-xl font-bold col-span-1">
                        Branch :
                    </label>
                    <select id="branch" name="branch" className="ml-3 col-span-3 appearance-none block w-full h-10 px-3 py-2 text-base border border-gray-400 rounded-full focus:outline-none focus:ring-indigo-500 focus:border-indigo-500">
                        <option value="">Select item</option>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                        <option value="3">Option 3</option>
                    </select>
                </div>
                <div className="w-full px-2 row-span-2 grid-cols-2 my-5 m-auto grid grid-cols-4">
                    <label className="block text-xl font-bold mt-2 col-span-1">
                        Username :
                    </label>
                    <input type="text" id="urername" name="username" className="ml-3 col-span-3 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"/>
                </div>
                <div className="w-full px-2 row-span-2 grid-cols-2 my-5 m-auto grid grid-cols-4">
                    <label className="block text-xl font-bold mt-2 col-span-1">
                        Phone :
                    </label>
                    <input type="number" id="urername" name="username" className="ml-3 col-span-3 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"/>
                </div>
            </div>
        </div>

    )
}
export default FitFood; 