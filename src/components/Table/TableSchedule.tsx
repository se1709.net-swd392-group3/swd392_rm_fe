import { useState } from "react";
import Modal from "./Modal";


const TableSchedule = () => {
    const [modalIsOpen, setIsOpen] = useState(false)

    const CloseModal = () =>{
        setIsOpen(false);
    }
    return (
        <div className="container mx-auto">
            <table className="table-auto w-full border border-collapse">
                <thead>
                    <tr className="bg-blue-200">
                        <th className="px-5 py-5 border border-collapse border-black">Employees</th>
                        <th className="px-5 py-5 border border-collapse border-black">Monday</th>
                        <th className="px-5 py-5 border border-collapse border-black">Tuesday</th>
                        <th className="px-5 py-5 border border-collapse border-black">Wednesday</th>
                        <th className="px-5 py-5 border border-collapse border-black">Thursday</th>
                        <th className="px-5 py-5 border border-collapse border-black">Friday</th>
                        <th className="px-5 py-5 border border-collapse border-black">Saturday</th>
                        <th className="px-5 py-5 border border-collapse border-black">Sunday</th>
                    </tr>
                </thead>
                <tbody className="text-center text-white bg-gray-800 text-lg">
                    <tr>
                        <td className="px-5 py-5 border border-collapse">
                            <button className="relative overflow-hidden sm:py-12" onClick={() => setIsOpen(true)}>
                                <label className="cursor-pointer rounded active:bg-slate-400">Jame</label>
                            </button>
                        </td>
                        <td className="px-5 py-5 border border-collapse">7-20</td>
                        <td className="px-5 py-5 border border-collapse">7-20</td>
                        <td className="px-5 py-5 border border-collapse">7-20</td>
                        <td className="px-5 py-5 border border-collapse">7-20</td>
                        <td className="px-5 py-5 border border-collapse">7-20</td>
                        <td className="px-5 py-5 border border-collapse">7-20</td>
                        <td className="px-5 py-5 border border-collapse">7-20</td>
                    </tr>
                </tbody>
            </table>

            <Modal isvisible={modalIsOpen} Close={CloseModal}/>
        </div >
    )
}

export default TableSchedule; 