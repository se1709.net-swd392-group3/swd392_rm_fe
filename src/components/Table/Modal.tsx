import React from "react";


const Modal = ({ isvisible, Close }) => {
    if(!isvisible) return null;

    return (
        <div className="fixed inset-0 Obg-black bg-opacity-25 backdrop-blur-sm flex justify-center items-center" >
            <div className="w-[600px] flex flex-col">
                <button onClick={() => Close()} className="text-white font-bold text-2xl place-self-end bg-green-500 w-full text-end pr-5 py-1">X</button>
                <div className="bg-white p-2 rounded text-center text-2xl font-bold">Time</div>
                <div className="grid grid-rows-2 bg-white p-2">
                    <div className="row-span-1 grid grid-cols-2">
                        <h2 className="text-2xl font-bold my-5">Start Time:</h2> 
                        <div>
                            <input type="number" id="startTime" name="startTime" className="appearance-none block w-full px-3 py-2 my-4 text-base border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" />
                        </div>
                    </div>
                    <div className="row-span-1 grid grid-cols-2">
                        <h2 className="text-2xl font-bold my-5">End Time:</h2> 
                        <div>
                            <input type="number" id="endTime" name="endTime" className="appearance-none block w-full px-3 py-2 my-4 text-base border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" />
                        </div>
                    </div>
                </div>
                <div className=" bg-white p-2">
                    <button onClick={() => Close()} className="text-white font-bold text-2xl place-self-end bg-green-500 w-1/3 text-center py-2 mx-48 rounded-lg">Update</button>
                </div>
            </div>
        </div>
    )
}
export default Modal;