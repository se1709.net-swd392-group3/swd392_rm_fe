import { Swiper, SwiperSlide } from 'swiper/react';
import menu from '../../Image/menu.jpg'

import 'swiper/css';

const ScrollList = () => {
    return (
        <div className='mt-20 h-10'>
            <Swiper
                slidesPerView={3}
                onSlideChange={() => console.log('slide change')}
                onSwiper={(swiper) => console.log(swiper)}
            >
                <SwiperSlide>
                    <div className='my-10 text-center shadow-black shadow-md rounded-xl'>
                        <img src={menu} className='w-2/3 h-2/3 mx-20' />
                        Food 1
                    </div>
                </SwiperSlide>
                <SwiperSlide>
                    <div className='my-10 text-center shadow-black shadow-md rounded-xl'>
                        <img src={menu} className='w-2/3 h-2/3 mx-20' />
                        Food 2
                    </div>
                </SwiperSlide>
                <SwiperSlide>
                    <div className='my-10 text-center shadow-black shadow-md rounded-xl'>
                        <img src={menu} className='w-2/3 h-2/3 mx-20' />
                        Food 3
                    </div>
                </SwiperSlide>
                <SwiperSlide>
                    <div className='my-10 text-center shadow-black shadow-md rounded-xl'>
                        <img src={menu} className='w-2/3 h-2/3 mx-20' />
                        Food 4
                    </div>
                </SwiperSlide>
                <SwiperSlide>
                    <div className='my-10 text-center shadow-black shadow-md rounded-xl'>
                        <img src={menu} className='w-2/3 h-2/3 mx-20' />
                        Food 5
                    </div>
                </SwiperSlide>
                <SwiperSlide>
                    <div className='my-10 text-center shadow-black shadow-md rounded-xl'>
                        <img src={menu} className='w-2/3 h-2/3 mx-20' />
                        Food 6
                    </div>
                </SwiperSlide>
            </Swiper>
        </div>
    )
}

export default ScrollList;