import { useState } from "react"
import { IProductSender } from "../../interfaces/Product"
import { AxiosResponse } from "axios"
import { API_R_200 } from "../../constants/codes"
import { ProductService } from "../../service/product.service"

const Food = () => {
    
    const [prodName, setProdName] = useState('')
    const [quantity, setQuantity] = useState(0)

    const handleChangeProdName = (e: React.ChangeEvent<HTMLInputElement>) => {
        setProdName(e.target.value)
    }

    const handleChangeQuantity = (e: React.ChangeEvent<HTMLInputElement>) => {
        let num = parseInt(e.target.value)
        if (isNaN(num)) {
            num = 0
        }
        setQuantity(num)
    }
    
    const onProdInfoSubmit = async () => {
        const data = {
            productName: prodName,
            quantity: quantity,
        } as IProductSender

        const response = (await ProductService.create(data)) as AxiosResponse
        if (response.status == API_R_200) {
            console.log('succeed')
            alert('Successfully created a product!')
        } else {
            alert('Failed to create a product!')
    }

    return (
        <div  className="py-10">
            <div className="flex flex-col bg-white rounded-lg shadow-md px-8 py-8 w-4/5 mx-auto my-20">
                <div className="flex items-center mb-4">
                    <h2 className="text-xl font-bold w-40">Food/Product Name:</h2>
                    <input type="text" value={prodName} onChange={handleChangeProdName} className="w-full px-3 py-2 ml-10 border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"/>
                </div>
                <div className="flex items-center mb-4">
                    <h2 className="text-xl font-bold w-40">Quantity:</h2>
                    <input type="text" value={quantity} onChange={handleChangeQuantity} className="w-full px-3 py-2 ml-10 border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"/>
                </div>
                <button onClick={onProdInfoSubmit} className="w-full py-2 px-4 bg-indigo-500 text-white font-bold rounded-md hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Create</button>
            </div>
        </div>
    )
}
export default Food;