import React, { useState } from "react";
import { ICourseSender } from "../../interfaces/Course";
import { CourseService } from "../../service/course.service";
import { AxiosResponse } from "axios";
import { API_R_200 } from "../../constants/codes";

const Course = () => {

    const [available, setAvailable] = useState(false)
    const [courseName, setCourseName] = useState('')
    const [price, setPrice] = useState(0)
    const [courseDesc, setCourseDesc] = useState('')
    const [imageFile, setImageFile] = useState<File>()

    const handleChangeCourseName = (e: React.ChangeEvent<HTMLInputElement>) => {
        setCourseName(e.target.value)
    }

    const handleChangePrice = (e: React.ChangeEvent<HTMLInputElement>) => {
        let num = parseInt(e.target.value)
        if (isNaN(num)) {
            num = 0
        }
        setPrice(num)
    }

    const handleChangeCourseDesc = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setCourseDesc(e.target.value)
    }

    const handleCheckAvail = (e: React.ChangeEvent<HTMLInputElement>) => {
        setAvailable(e.target.checked)
    }

    const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files)
            setImageFile(e.target.files[0])
    }

    const onCourseInfoSubmit = async () => {
        const data = {
            courseName: courseName,
            price: price,
            description: courseDesc,
            available: available,
            image: imageFile?.name
        } as ICourseSender

        const response = (await CourseService.create(data)) as AxiosResponse
        if (response.status == API_R_200) {
            console.log('succeed')
            alert('Successfully created a course!')
        } else {
            alert('Failed to create a course!')
        }
    }

    return (
        <div  className="py-10">
            <div className="flex flex-col bg-white rounded-lg shadow-md px-8 py-8 w-4/5 mx-auto my-20">
                <div className="flex items-center mb-4">
                    <h2 className="text-xl font-bold w-40">Course Name:</h2>
                    <input type="text" value={courseName} onChange={handleChangeCourseName} className="w-full px-3 py-2 ml-10 border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"/>
                </div>
                <div className="flex items-center mb-4">
                    <h2 className="text-xl font-bold w-40">Price:</h2>
                    <input type="text" value={price} onChange={handleChangePrice} className="w-full px-3 py-2 ml-10 border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"/>
                </div>
                <div className="mb-4">
                    <h2 className="text-xl font-bold mb-2">Description:</h2>
                    <textarea  value={courseDesc} onChange={handleChangeCourseDesc} className="w-full px-3 py-2 border border-gray-400 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"/>
                </div>
                <div className="mb-4">
                    <h2 className="text-xl font-bold mb-2">Available:</h2>
                    <input type="checkbox" checked={available} onChange={handleCheckAvail} className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"/>
                </div>
                <div className="flex items-center mb-4">
                    <h2 className="text-xl font-bold w-40">Image:</h2>
                    <div className="flex flex-col w-full ml-10">
                        <label className="relative cursor-pointer rounded-md bg-gray-200 hover:bg-gray-300 px-6 py-5">
                            <input onChange={handleFileChange} id="course-image" type="file" className="sr-only"/>
                                <span className="text-sm font-medium text-gray-500"> Choose file </span>
                        </label>
                        <p className="text-xs text-gray-500 mt-2">{imageFile ? imageFile.name : 'No file chosen yet'}</p>
                    </div>
                </div>
                <button onClick={onCourseInfoSubmit} className="w-full py-2 px-4 bg-indigo-500 text-white font-bold rounded-md hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Create</button>
            </div>
        </div>
    )
}
export default Course;