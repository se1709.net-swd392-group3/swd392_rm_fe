import { AxiosError } from "axios";
import { API_ENV, API_PRODUCT_CREATE } from "../constants/endpoints";
import { BaseService } from "./base.service";
import { IProductSender } from "../interfaces/Product";

export class ProductService extends BaseService {
    static async create(data: IProductSender) {
        try {
            const response = await this.request({ auth: true }).post(
              API_ENV.MAIN + API_PRODUCT_CREATE,
              data
            )
            console.log(response)
            return response
          } catch (error: any) {
            return (error as AxiosError).response
          }
      
    }
}