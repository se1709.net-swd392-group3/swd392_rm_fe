import { AxiosError } from "axios"
import { API_ENV, API_SCHEDULE_UPDATE } from "../constants/endpoints"
import { BaseService } from "./base.service"
import { ISchedule } from "../interfaces/Schedule"

export class ScheduleService extends BaseService {
    static async update(data: ISchedule) {
        try {
            const response = await this.request({ auth: true }).post(
              API_ENV.MAIN + API_SCHEDULE_UPDATE,
              data
            )
            console.log(response)
            return response
          } catch (error: any) {
            return (error as AxiosError).response
          }
      
    }
}