import { AxiosError } from "axios";
import { API_ENV, API_ORDER_CREATE } from "../constants/endpoints";
import { BaseService } from "./base.service";
import { IOrder } from "../interfaces/Order";

export class OrderService extends BaseService {

  static async create(payload: IOrder) {
    try {
        const response = await this.request({ auth: true }).post(
          API_ENV.MAIN + API_ORDER_CREATE,
          payload
        )
        console.log(response)
        return response
      } catch (error: any) {
        return (error as AxiosError).response
      }
  
  }
}