import { AxiosError } from "axios";
import { API_ENV, API_FEEDBACK_SUBMIT } from "../constants/endpoints";
import { IFeedback } from "../interfaces/Feedback";
import { BaseService } from "./base.service";

export class FeedbackService extends BaseService {
    static async submitFeedback(data: IFeedback) {
        try {
            const response = await this.request({ auth: true }).post(
              API_ENV.MAIN + API_FEEDBACK_SUBMIT,
              data
            )
            console.log(response)
            return response
          } catch (error: any) {
            return (error as AxiosError).response
          }
      
    }
}