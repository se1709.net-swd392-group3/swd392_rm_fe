import { AxiosError } from "axios"
import { ICourse } from "../interfaces/Course"
import { BaseService } from "./base.service"
import { API_COURSE_CREATE, API_ENV } from "../constants/endpoints"

export class CourseService extends BaseService {
    static async create(data: ICourse) {
        try {
            const response = await this.request({ auth: true }).post(
              API_ENV.MAIN + API_COURSE_CREATE,
              data
            )
            console.log(response)
            return response
          } catch (error: any) {
            return (error as AxiosError).response
          }
      
    }
}