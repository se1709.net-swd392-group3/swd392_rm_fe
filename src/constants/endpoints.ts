// API ENDPOINTS
export const API_ENV = {
    MAIN: "/api"
}

// ACCOUNT RELATED
export const API_ACCOUNT_LOG_IN = '/v1/auth/login' // post
//

// PRODUCT RELATED
export const API_PRODUCT_CREATE = '/v1/admin/products/create'

// FEEDBACK RELATED
export const API_FEEDBACK_SUBMIT = '/v1/admin/feedbacks'

// SCHEDULE
export const API_SCHEDULE = '/v1/admin/schedules'

// ORDER
export const API_ORDER_CREATE = '/v1/admin/orders'
export const API_ORDER_GET = '/v1/admin/orders'

export const API_COURSE_CREATE  = '/v1/admin/courses'

export const API_SCHEDULE_UPDATE  = '/v1/admin/schedules'