import HeaderComponent from "../components/Base/HeaderComponent"
import FooterComponent from "../components/Base/FooterComponent"

interface Props {
    children: React.ReactNode
}

const DefaultLayout: React.FC<Props> = (props) => {
    return (
        <div className="bg-gray-300">
            <header>
                <HeaderComponent/>
            </header>
            <div>
                {props.children}
            </div>
            <footer>
                <FooterComponent/>
            </footer>
        </div>
    )
}

export default DefaultLayout