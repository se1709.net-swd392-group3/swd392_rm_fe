import {BrowserRouter, Route, Routes} from 'react-router-dom'

import HomePage from './pages/HomePage'
import LoginPage from './pages/auth/LoginPage'
import ManagerSchedule from './pages/ManagerSchedule'
import Order from './pages/Order'
import CoursePage from './pages/Course'
import FeedBack from './pages/FeedBack'
import FoodPage from './pages/Food'
import { Logout } from './pages/auth/Logout'
import { ProtectedRoute } from './components/Common/ProtectedRoute'

function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage/>}/> {/* 👈 Renders at /app/ */}
        <Route path="/auth/login" element={<LoginPage/>}/>
        <Route path='/ManagerSchedule' element={<ProtectedRoute><ManagerSchedule/></ProtectedRoute>}/>
        <Route path='/Order' element={<Order/>}/>
        <Route path='/Course' element={<ProtectedRoute><CoursePage/></ProtectedRoute>}/>
        <Route path='/FeedBack' element={<FeedBack/>}/>
        <Route path='/Food' element={<ProtectedRoute><FoodPage/></ProtectedRoute>}/>
        <Route path='/auth/logout' element={<Logout/>}/>
      </Routes>
    </BrowserRouter>
  )
}

export default App
