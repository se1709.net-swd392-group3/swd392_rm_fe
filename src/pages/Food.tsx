import Food from "../components/Food/Food";
import DefaultLayout from "../layouts/DefaultLayout"

const FoodPage: React.FC = () => {
    return (
        <DefaultLayout>
            <div className="w-full bg-gray-300 pt-20">
                <div className="text-5xl font-extrabold text-amber-500 decoration-dotted decoration-black underline italic w-full mt-10">
                    <h1 className="ml-20">Add Food</h1>
                </div>

                <div>
                    <Food/>
                </div>
            </div>
        </DefaultLayout>
    )
}

export default FoodPage;