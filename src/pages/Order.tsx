import { Button } from "@material-tailwind/react";
import FitFood from "../components/Card/FitFood";
import OrderForm from "../components/Card/OrderForm";
import ScrollList from "../components/ScrollList/ScrollList";
import DefaultLayout from "../layouts/DefaultLayout"
import { useState } from "react";
import ModalAlert from "../components/Card/ModalAlert";

const Order: React.FC = () => {
    const [modalIsOpen, setIsOpen] = useState(false)

    const CloseModal = () =>{
        setIsOpen(false);
    }
    
    return (
        <DefaultLayout>
            <div className="w-full bg-gray-300 pt-20">
                <div className="text-6xl font-extrabold text-amber-500 decoration-dotted decoration-black underline italic text-center w-full mt-10 ">
                    <h1 className="">====== List Food ======</h1>
                </div>

                <div className="bg-teal-100 h-80 rounded-3xl">
                    <div className="h-full mx-10">
                        <ScrollList />
                    </div>
                </div>

                <div className="text-6xl font-extrabold text-amber-500 italic text-center w-full mt-40 ">
                    <h1 className="">Fit Food - Order form :</h1>
                </div>

                <div className="py-5">
                    <FitFood/>
                </div>

                <div className="pb-10">
                    <OrderForm/>
                </div>

                <div className="justify-items-end content-end">
                    <div className="w-full ml-80">
                        <Button onClick={() => {setIsOpen(true)}} about="" placeholder="Feed Back" className="w-60 h-30 mx-20 ml-80 text-xl text-white my-10 bg-green-600 font-bold">
                            Create
                        </Button>
                    </div>
                </div>
                <ModalAlert Close={() => {setIsOpen(false)}} isvisible={modalIsOpen}/>
            </div>
        </DefaultLayout>
    )
}
export default Order;