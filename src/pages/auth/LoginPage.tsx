import { Button, Input, Typography } from "@material-tailwind/react"
import DefaultLayout from "../../layouts/DefaultLayout"
import { AuthService } from "../../service/auth.service"
import React, { useEffect, useState } from "react"
import { IAccountLogin } from "../../interfaces/Account"
import { AxiosResponse } from "axios"
import { API_R_200 } from "../../constants/codes"
import { StorageUtils } from "../../utils/storage.utils"
import { useNavigate } from "react-router-dom"
import { CommonUtils } from "../../utils/common.utils"


const LoginPage : React.FC = (props: any) => {

    useEffect(() => {
        checkIfTheresUser()
     });

    const navigate = useNavigate();

    const checkIfTheresUser = () => {
        const user = CommonUtils.getUserEmail()
        if (user != null) {
            navigate('/')
        }
    }

    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')

    const handleOnLogin = async () => {
        
        const data = {
            username: username,
            password: password
        } as IAccountLogin

        const response = await (AuthService.login(data)) as AxiosResponse
        if (response.status == API_R_200) {
            console.log(import.meta.env.APP_ENCRYPTION_KEY)
            StorageUtils.setItem('tokenInfo', JSON.stringify(response!.data.data))
            console.log("login success!")
            navigate('/')
        }
    }

    const handleUsernameInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        setUsername(e.target.value)
    }

    const handlePasswordInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(e.target.value)
    }

    return (
        <DefaultLayout>
            <div className="w-3/5 mx-52 py-5 my-52 h-auto bg-gray-100 rounded-xl px-20">
                <Typography placeholder={''} variant="h2">Login</Typography>
                <Input value={username} onChange={handleUsernameInput} label="Username"/>
                <Input type="password" onChange={handlePasswordInput} value={password} label="Password"/>
                <Button title="Login" onClick={handleOnLogin} placeholder="Login" className="w-40 h-10 mt-4 mx-10">Login</Button>
            </div>
        </DefaultLayout>
    )
}

export default LoginPage