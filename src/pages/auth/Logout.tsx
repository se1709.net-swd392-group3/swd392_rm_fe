import { useEffect } from "react"
import { AuthService } from "../../service/auth.service"
import { StorageUtils } from "../../utils/storage.utils"

export const Logout : React.FC = (props: any) => {
    useEffect(() => {
        StorageUtils.clearItems()
    })
}