import GiveFeedBacl from "../components/Card/GiveFeedBack";
import DefaultLayout from "../layouts/DefaultLayout";

const FeedBack:React.FC = () =>{
    return(
        <DefaultLayout>
            <div className="w-full bg-gray-300 pt-20">
                <div>
                    <div className="text-5xl font-extrabold text-amber-500 decoration-dotted decoration-black underline italic w-full mt-10">
                        <h1 className="ml-20">Order Information</h1>
                    </div>
                    <div className="text-2xl font-extrabold italic w-full mt-10">
                        <div className="grid grid-cols-7 gap-x-8 items-center ml-20">
                            <div className="col-span-1 text-green-800">Order ID: </div>
                            <div className="col-span-6 text-left">123456789</div>
                        </div>
                        <div className="grid grid-cols-7 gap-x-8 items-center ml-20">
                            <div className="col-span-1 text-green-800">Create Time: </div>
                            <div className="col-span-6 text-left">January 1, 2024, 12:00 PM</div>
                        </div>
                        <div className="grid grid-cols-7 gap-x-8 items-center ml-20">
                            <div className="col-span-1 text-green-800">Price:</div>
                            <div className="col-span-6 text-left">$25,99</div>
                        </div>
                    </div>
                </div>

                <div className="py-10">
                    <GiveFeedBacl/>
                </div>
            </div>
        </DefaultLayout>
    )
}
export default FeedBack;