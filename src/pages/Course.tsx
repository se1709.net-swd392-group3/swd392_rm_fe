import Course from "../components/Course/Course";
import DefaultLayout from "../layouts/DefaultLayout"

const CoursePage: React.FC = () => {
    return (
        <DefaultLayout>
            <div className="w-full bg-gray-300 pt-20">
                <div className="text-5xl font-extrabold text-amber-500 decoration-dotted decoration-black underline italic w-full mt-10">
                    <h1 className="ml-20">Add Course</h1>
                </div>

                <div>
                    <Course/>
                </div>
            </div>
        </DefaultLayout>
    )
}
export default CoursePage;