import React from "react"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faStar } from "@fortawesome/free-regular-svg-icons"

import DefaultLayout from "../layouts/DefaultLayout"
import bg from "../Image/bg.jpg"
import ScrollList from "../components/ScrollList/ScrollList"
import IntroMenu from "../components/Card/IntroMenu"
import IntroChef from "../components/Card/IntroChef"
import FeedBackCard from "../components/Card/FeedBack"


const HomePage: React.FC = () => {
    return (
        <DefaultLayout>
            <div className="snap-y snap-center bg-gray-300">
                <div className="w-full">
                    <img src={bg} className="w-full h-96" />
                </div>

                <div className="text-7xl font-extrabold text-amber-500 decoration-dotted decoration-black underline italic text-center w-full mt-10 ">
                    <h1 className="">Restaurant</h1>
                    <div>
                        <FontAwesomeIcon icon={faStar} className="w-10 h-10" />
                        <FontAwesomeIcon icon={faStar} className="w-10 h-10" />
                        <FontAwesomeIcon icon={faStar} className="w-10 h-10" />
                    </div>
                </div>

                <div className="bg-teal-100 h-80 rounded-3xl">
                    <div className="h-full mx-10">
                        <ScrollList />
                    </div>
                </div>

                <div className="my-10 grid grid-cols-2 w-full h-full">
                    <div className="col-span-1 grid grid-rows-2">
                        <div className="row-span-1 w-full my-10 ml-28">
                            <IntroMenu />
                        </div>
                        <div className="row-span-1 my-10 ml-28">
                            <FeedBackCard />
                        </div>
                    </div>
                    <div className="col-span-1 my-40">
                        <IntroChef />
                    </div>
                </div>
            </div>
        </DefaultLayout>
    )
}

export default HomePage