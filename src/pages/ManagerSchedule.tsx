import { Button } from "@material-tailwind/react";
import DefaultLayout from "../layouts/DefaultLayout";
import TableSchedule from "../components/Table/TableSchedule";

const ManagerSchedule: React.FC = () => {
    return (
        <DefaultLayout>
            <div className="w-full bg-gray-300 pt-20">
                <div className="text-7xl font-extrabold text-amber-500 decoration-dotted decoration-black underline italic text-center w-full mt-10 ">
                    <h1 className="">Manager Schedule</h1>
                </div>

                <div className="grid grid-cols-7 justify-items-end mr-10 mt-40">
                    <Button about="" placeholder="Feed Back" className="col-span-5 w-60 h-30 text-xl text-white my-10 bg-green-600 font-bold">
                        Update
                    </Button>
                    <Button about="" placeholder="Feed Back" className="col-span-2 w-60 h-30 text-xl text-white my-10 ml-20 bg-gray-600 font-bold">
                        Clear
                    </Button>
                </div>

                <div className="pb-20">
                    <TableSchedule/>
                </div>
            </div>
        </DefaultLayout>
    )
}
export default ManagerSchedule; 