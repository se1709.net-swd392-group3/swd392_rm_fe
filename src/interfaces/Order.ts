import { IAccount } from "./Account"
import { ICourse } from "./Course"
import { IFeedback } from "./Feedback"

export interface IOrderCourseMap {
    id: number,
    course: ICourse,
    order: IOrder
}

export interface IOrder {
    id: number,
    orderName: string,
    createdTime: Date,
    orderCourseMaps: IOrderCourseMap[]
    feedback: IFeedback[]
    user: IAccount
}