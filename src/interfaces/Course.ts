export interface ICourseSender {
    id?: number
    courseName: string
    price: number
    description: string
    image: string
    available: boolean
}