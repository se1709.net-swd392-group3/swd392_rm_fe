export interface ISchedule {
    id: number,
    scheduleName: string,
    startTime: Date,
    endTime: Date
}