export interface IProduct {
    id: number
    productName: string
    quantity: number
    //image: string
}

export interface IProductSender {
    id?: number
    productName: string
    quantity: number
    //image: string
}