import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  envPrefix: 'APP_',
  server: {
    
    proxy: {
      '/api': {
        target: 'http://localhost:8000/rm',
        changeOrigin: true,
        secure: false,
      }
    }
    
  },
  build: {
    chunkSizeWarningLimit:  500
  },
  define: {
    global: {},
  }
})
